<?php 


	$NumOperacion = rand(10000,99999);

	$claveEncriptacion = "87401456";

	$MerchantID = "082108630";
	$AcquirerBIN = "0000554002";
	$TerminalID = "00000003";
	$Importe = "50000";
	$TipoMoneda = "978";
	$Exponente = "2";
	$Cifrado = "SHA2";
	$URLOK = "http://localhost/maestria/pasarela-pago/prueba.php?p=1";
	$URLNOK = "http://localhost/maestria/pasarela-pago/prueba.php?p=2";
	$ExtensionSCA = "";

	date_default_timezone_set('America/Mexico_City');
	$datosACS = array(
		"ciudad" 	=> "Demo",
		"pais"		=> "MX",
		"dir1"		=> "Demo 1",
		"cp"		=> "11520",
		"estado"	=> "DF",
		"cliente"	=> "Demo Demo",
		"emailc"	=> "demo@ismaias.com",
		"celc"		=> "5532232121",
		//"hoy"		=> date('Y-m-d h:i:s', time())
		"hoy"		=> "2021-01-27 02:12:01"
	);

	
	/*
	$data1 = "998888881119500280000554052000000031235009782SHA2http://www.ceca.eshttp://www.ceca.es";
	echo $data1;
	echo "<hr />";
	*/


	$data = $claveEncriptacion.$MerchantID.$AcquirerBIN.$TerminalID.$NumOperacion.$Importe.$TipoMoneda.$Exponente.$Cifrado.$URLOK.$URLNOK.$ExtensionSCA;
	echo "<hr /><h3>Datos para hacer el hash</h3>";
	echo $data;



	function makeHash($message, $replace = true)
    {

        if ( $replace ) {
            $message = str_replace('&amp;', '&', $message);
        }
        return hash('sha256', $message);

    }


	echo "<hr /><h3>Firma</h3>";

	echo makeHash($data);

	/*
	 Datos ACS
	*/

	$cardholder = array(
		"BILL_ADDRESS" => array(
			"CITY" 		=> $datosACS["ciudad"],
			"COUNTRY" 	=> $datosACS["pais"],
			"LINE1" 	=> $datosACS["dir1"],
			"POST_CODE" => $datosACS["cp"],
			"STATE"		=> $datosACS["estado"]
		),
		"NAME"	=> $datosACS["cliente"],
		"EMAIL"	=> $datosACS["emailc"]
	);

	$purchase = array(
		"SHIP_ADDRESS"	=> array(
			"CITY"		=> $datosACS["ciudad"],
			"COUNTRY"	=> $datosACS["pais"],
			"LINE1" 	=> $datosACS["dir1"],
			"POST_CODE" => $datosACS["cp"],
			"STATE"		=> $datosACS["estado"]
		),
		"MOBILE_PHONE" => array(
			"SUBSCRIBER"=> $datosACS["celc"]
		)
	);

	$merchant_risk = array(
		"PRE_ORDER_PURCHASE_IND" 	=> "AVAILABLE",
		"SHIP_INDICATOR"			=> "CH_BILLING_ADDRESS",
		"DELIVERY_TIMEFRAME"		=> "TWO_MORE_DAYS",
		"REORDER_ITEMS_IND"			=> "FIRST_TIME_ORDERED"
	);

	$account_info = array(
		"SUSPICIOUS_ACC_ACTIVITY"	=> "NO_SUSPICIOUS",
		"CH_ACC_AGE_IND"			=> "LESS_30",
		"PAYMENT_ACC_IND"			=> "LESS_30",
		"CH_ACC_CHANGE_IND"			=> "LESS_30",
		"CH_ACC_CHANGE"				=> $datosACS["hoy"],
		"CH_ACC_DATE"				=> $datosACS["hoy"],
		"PAYMENT_ACC_AGE"			=> $datosACS["hoy"],
		"TXN_ACTIVITY_DAY"			=> 1,
		"TXN_ACTIVITY_YEAR"			=> 10,
		"SHIP_NAME_INDICATOR"		=> "DIFFERENT"
	);

 	$acs = array();
 	$acs['CARDHOLDER'] = $cardholder;
 	$acs['PURCHASE'] = $purchase;
 	$acs['MERCHANT_RISK_IND'] = $merchant_risk;
 	$acs['ACCOUNT_INFO'] = $account_info;

 	echo "<hr /><h3>ACS20</h3>";
 	print_r($acs);


 	$acsready = urlencode(json_encode( $acs ));

 	echo "<hr /><h3>Datos encripatados</h3>";
 	echo $acsready;


 	/*
 		Firma ACS 20
 	*/
 	
 	$firmaacs20 = makeHash($acsready, false);
 	echo "<hr /><h3>Firma datos acs20</h3>";
 	echo $firmaacs20;

?>

<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<title></title>
		<style>
			body {
				font-family: Arial;
			}
		</style>
	</head>
	<body>
		<h1>Formulario</h1>

		<form action="https://tpv.ceca.es/tpvweb/tpv/compra.action" method="post">

			<input name="MerchantID" type="hidden" value="<?php echo $MerchantID ?>" />
			<input name="AcquirerBIN" type="hidden" value="<?php echo $AcquirerBIN ?>" />
			<input name="TerminalID" type="hidden" value="<?php echo $TerminalID ?>" />
			<input name="URL_OK" type="hidden" value="<?php echo $URLOK ?>" />
			<input name="URL_NOK" type="hidden" value="<?php echo $URLNOK ?>" />
			<input name="Cifrado" type="hidden" value="<?php echo $Cifrado ?>" />
			<input name="Num_operacion" type="hidden" value="<?php echo $NumOperacion ?>" />
			<input name="Importe" type="hidden" value="<?php echo $Importe ?>" />
			<input name="TipoMoneda" type="hidden" value="<?php echo $TipoMoneda ?>" />
			<input name="Exponente" type="hidden" value="<?php echo $Exponente ?>" />
			<input name="Pago_soportado" type="hidden" value="SSL" />
			<input name="Idioma" type="hidden" value="1" />
			<input name="Firma" type="hidden" value="<?php echo makeHash($data) ?>" />
			<input name="datos_acs_20" type="hidden" value="<?php echo $acsready ?>" />
			<input name="firma_acs_20" type="hidden" value="<?php echo $firmaacs20 ?>" />
			<input name="Descripcion" type="hidden" value="Pago del pedido <?php echo $NumOperacion ?>" />

			<button type="submit">
				Pagar
			</button>
		</form>
	</body>
</html>