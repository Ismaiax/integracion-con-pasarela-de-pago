<?php

    $productos = array(
        array(
            "nombre"    => "Comida para perros",
            "desc"      => "Comida para perros de 12 meses a 7 años",
            "precio"    => 16.50
        ),
        array(
            "nombre"    => "Comida para gatos",
            "desc"      => "Para el gato feliz",
            "precio"    => 18.90
        ),
    );

    if(isset($_GET['m'])) {
        $moneda = array(
            "tipo" => "euro"
        );
    } else {
        $moneda = array(
            "tipo" => "peso"
        );
    }


    function converterToEuro($x, $m = true){
        $euro = 24.59;
        if($m){
            $x = number_format( (float)($x / $euro), 2, '.', '' );
        } else {
            $x = number_format( (float)($x * 1), 2, '.', '' );
        }
        return $x;
    }


?>


<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>Tienda de Ismaias - Página de compra</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script> 
    
        <style>
            .card {
                background-color: #80ff001f;
            }
        </style>
    </head>
    <body>

        <header>
            <div class="collapse bg-dark" id="navbarHeader">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-7 py-4">
                            <h4 class="text-white">Tarea</h4>
                            <p class="text-muted">
                                Implementación de la pasarela de pago Cecabank para una tienda en línea básica.<br/>
                                <b>Alumno:</b> Isaías Ismael Tello Lara<br />
                                <b>Materia:</b> Modelos de negocio y métodos de pago
                            </p>
                        </div>
                        <div class="col-sm-4 offset-md-1 py-4">
                            <h4 class="text-white">Contacto</h4>
                            <ul class="list-unstyled">
                                <li><a href="mailto:isaiasismael.telloli8@comunidadunir.net" class="text-white">Correo electrónico</a></li>
                                <li><a href="tel:+5560420790" class="text-white">Celular</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-dark bg-dark shadow-sm">
                <div class="container">
                    <a href="#" class="navbar-brand d-flex align-items-center">
                        <strong>Tienda de Ismael Tello</strong>
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </header>


        <main>
            <section class="py-5 text-center container">
                <div class="row py-lg-2">
                    <div class="col-lg-7 col-md-9 mx-auto">
                        <h1 class="fw-light">Modo de uso</h1>
                        <p class="lead text-muted">Selecciona el tipo de moneda, llena el formulario y ve de compras</p>
                        <div class="mb-2">
                            <div class="btn-group" role="group">
                                <a href="./" class="btn chcurrency <?php echo $moneda["tipo"] == "peso" ? "btn-primary" : "btn-light" ?>" data-currency="peso">Peso mexicano</a>
                                <a href="./?m" class="btn chcurrency <?php echo $moneda["tipo"] == "euro" ? "btn-primary" : "btn-light" ?>" data-currency="euro">Euro</a>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>


            <form class="container" action="p.php" autocomplete="off" method="post">
                <div class="row">
                    
                    <!-- Formulario  -->
                    <div class="col-12 col-lg-5 mb-4">
                        
                            <h3>Rellena el formulario</h3>
                            <div class="row mb-2">
                                <div class="col-12 col-lg-6">
                                    <label for="nombrecliente" class="form-label">Nombre</label>
                                    <input autofocus class="form-control" id="nombrecliente" name="nombrecliente" placeholder="Juan Perez" required tabindex="1" type="text">
                                </div>
                                <div class="col-12 col-lg-6">
                                    <label for="correocliente" class="form-label">Email</label>
                                    <input class="form-control" id="correocliente" name="correocliente" placeholder="juan@perez.com" required tabindex="2" type="email">
                                </div>
                            </div>
                            <div class="mb-2">
                                <label for="telefonocliente" class="form-label">Teléfono</label>
                                <input class="form-control" id="telefonocliente" name="telefonocliente" placeholder="55 9090 8989" required tabindex="3" type="tel">
                            </div>
                            
                            <!-- Dirección -->
                            <div class="row mb-2">
                                <div class="col-4">
                                    <label for="paiscliente" class="form-label">País</label>
                                    <input class="form-control" id="paiscliente" name="paiscliente" readonly required type="tel" value="MX">
                                </div>
                                <div class="col-8">
                                    <label for="estadocliente" class="form-label">Estado</label>
                                    <input class="form-control" list="listadeestados" id="estadocliente" name="estadocliente" placeholder="Busca tu estado..." required tabindex="4">
                                    <datalist id="listadeestados">
                                        <option value="Aguascalientes">
                                        <option value="Baja California">
                                        <option value="Baja California Sur">
                                        <option value="Campeche">
                                        <option value="Chiapas">
                                        <option value="Chihuahua">
                                        <option value="Coahuila">
                                        <option value="Colima">
                                        <option value="CDMX">
                                        <option value="Durango">
                                        <option value="Estado de México">
                                        <option value="Guanajuato">
                                        <option value="Guerrero">
                                        <option value="Hidalgo">
                                        <option value="Jalisco">
                                        <option value="Michoacán">
                                        <option value="Morelos">
                                        <option value="Nayarit">
                                        <option value="Nuevo León">
                                        <option value="Oaxaca">
                                        <option value="Puebla">
                                        <option value="Querétaro">
                                        <option value="Quintana Roo">
                                        <option value="San Luis Potosí">
                                        <option value="Sinaloa">
                                        <option value="Sonora">
                                        <option value="Tabasco">
                                        <option value="Tamaulipas">
                                        <option value="Tlaxcala">
                                        <option value="Veracruz">
                                        <option value="Yucatán">
                                        <option value="Zacatecas">
                                    </datalist>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-12 col-lg-6">
                                    <label for="ciudadcliente" class="form-label">Ciudad</label>
                                    <input class="form-control" id="ciudadcliente" name="ciudadcliente" placeholder="" required tabindex="5" type="text">
                                </div>
                                <div class="col-12 col-lg-6">
                                    <label for="codigocliente" class="form-label">Código postal</label>
                                    <input class="form-control" id="codigocliente" name="codigocliente" placeholder="55666" required tabindex="6" type="text">
                                </div>
                            </div>
                            <div>
                                <label for="direccioncliente" class="form-label">Dirección</label>
                                <input class="form-control" id="direccioncliente" name="direccioncliente" placeholder="Calle, número, interior" required tabindex="7" type="text">
                            </div>



                    </div>

                    <!-- Carrito -->
                    <div class="col-12 offset-lg-1 col-lg-6 mb-4">
                        <h3>Vayamos de compras</h3>

                        <?php for ($i=0; $i < count($productos); $i++) {  ?>
                            <div class="card mb-3">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $productos[$i]["nombre"] ?></h5>
                                    <div class="row">
                                        <div class="col">
                                            <b>Precio</b> $<span id="unidad-producto-<?php echo ($i+1) ?>">
                                                <?php
                                                    if($moneda['tipo']=="euro"){
                                                        echo converterToEuro($productos[$i]["precio"]);
                                                    } else {
                                                        echo converterToEuro($productos[$i]["precio"], false);
                                                    }
                                                ?>
                                            </span><br/>
                                            <div class="input-group">
                                                <span class="input-group-text">Subtotal</span>
                                                <span class="input-group-text total-producto" id="total-producto-<?php echo ($i+1) ?>" style="background-color:white; min-width:35%">0</span>
                                            </div>
                                        </div>
                                        <div class="col">
                                            Cantidad: 
                                            <select class="form-select select-producto" data-source="unidad-producto-<?php echo ($i+1) ?>" data-target="total-producto-<?php echo ($i+1) ?>" tabindex="<?php echo (($i+1)+7) ?>">
                                                <option value="0" selected>0</option>
                                                <?php for($k=1; $k < 10; $k++) { ?>
                                                    <option value="<?php echo $k ?>"><?php echo $k ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="mb-2">
                            <h2>Total a pagar: $<span id="totalCarrito">0</span></h2>
                            <input id="totalCarritoInput" name="totalCarritoInput" type="hidden" value="0" />
                        </div>

                        <p class="text-muted">No olvides ver los datos para pagar, <a href="https://comercios.ceca.es/docs_constpv/img/manual_comercios.pdf#page=23" target="_blank">aquí</a></p>
                        <button type="submit" class="btn btn-success btn-lg mt-1 mb-5">
                            Procesar pago
                        </button>

                    </div>
                </div>
            </form>

        </main>

        <script>
            $(function() {
                console.log('Comienza la magia');

                function financial(x) {
                    return Number.parseFloat(x).toFixed(2);
                }

                $('[class*="select-producto"]').on('change', function(e){
                    var cantidad = $(this).val();
                    var origen = $(this).data('source');
                    var destino = $(this).data('target'); 
                    var total = 0;
                    var totalEuro = 0;

                    $('#'+destino).text( financial( ( $('#'+origen).text() * cantidad ) ) );


                    $.each( $('.total-producto'), function(index,value) {

                        total += parseFloat($(value).text());

                    } );
                    $('#totalCarrito').text(financial(total));

                    <?php if(isset($_GET['m'])){ ?>
                        totalEuro = total;
                    <?php } else { ?>
                        totalEuro = total/24.59;
                    <?php } ?>

                    $('#totalCarritoInput').val(financial(totalEuro));

                });

            });
        </script>


    </body>
</html>



