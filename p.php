<?php

    function makeHash($message, $replace = true) {
        if ( $replace ) {
            $message = str_replace('&amp;', '&', $message);
        }
        return hash('sha256', $message);
    }


    $orden = rand(10000,99999);

    $configuracion = array(

        "SecretKey"         => "87401456",
        "Transaccion"       => $orden,
        "MerchantID"        => "082108630",
        "AcquirerBIN"       => "0000554002",
        "TerminalID"        => "00000003",
        "Importe"           => str_replace(".","",$_POST["totalCarritoInput"]),
        "TipoMoneda"        => "978",
        "Exponente"         => "2",
        "Cifrado"           => "SHA2",
        "URLOK"             => "https://www.ismaias.com/maestria/semestre-2/modelos-de-negocio-formas-pago/pasarela-pago2/compra.php?p=1&t=".$orden,
        "URLNOK"            => "https://www.ismaias.com/maestria/semestre-2/modelos-de-negocio-formas-pago/pasarela-pago2/compra.php?p=2",
    
    );

    //print_r($configuracion);

    $datosACS = array(
		"ciudad" 	=> $_POST["ciudadcliente"],
		"pais"		=> $_POST["paiscliente"],
		"dir1"		=> $_POST["direccioncliente"],
		"cp"		=> $_POST["codigocliente"],
		"estado"	=> $_POST["estadocliente"],
		"cliente"	=> $_POST["nombrecliente"],
		"emailc"	=> $_POST["correocliente"],
		"celc"		=> $_POST["telefonocliente"],
		"hoy"		=> "2021-01-27 02:12:01"
    );
    

    /*
        Hacemos el firmado
    */
    $firma = $configuracion["SecretKey"].$configuracion["MerchantID"].$configuracion["AcquirerBIN"].$configuracion["TerminalID"].$configuracion["Transaccion"].$configuracion["Importe"].$configuracion["TipoMoneda"].$configuracion["Exponente"].$configuracion["Cifrado"].$configuracion["URLOK"].$configuracion["URLNOK"];

    $firma = makeHash($firma);

    /*
        Hacemos los datos ACS 20
    */
    $cardholder = array(
		"BILL_ADDRESS" => array(
			"CITY" 		=> $datosACS["ciudad"],
			"COUNTRY" 	=> $datosACS["pais"],
			"LINE1" 	=> $datosACS["dir1"],
			"POST_CODE" => $datosACS["cp"],
			"STATE"		=> $datosACS["estado"]
		),
		"NAME"	=> $datosACS["cliente"],
		"EMAIL"	=> $datosACS["emailc"]
	);

	$purchase = array(
		"SHIP_ADDRESS"	=> array(
			"CITY"		=> $datosACS["ciudad"],
			"COUNTRY"	=> $datosACS["pais"],
			"LINE1" 	=> $datosACS["dir1"],
			"POST_CODE" => $datosACS["cp"],
			"STATE"		=> $datosACS["estado"]
		),
		"MOBILE_PHONE" => array(
			"SUBSCRIBER"=> $datosACS["celc"]
		)
	);

	$merchant_risk = array(
		"PRE_ORDER_PURCHASE_IND" 	=> "AVAILABLE",
		"SHIP_INDICATOR"			=> "CH_BILLING_ADDRESS",
		"DELIVERY_TIMEFRAME"		=> "TWO_MORE_DAYS",
		"REORDER_ITEMS_IND"			=> "FIRST_TIME_ORDERED"
	);

	$account_info = array(
		"SUSPICIOUS_ACC_ACTIVITY"	=> "NO_SUSPICIOUS",
		"CH_ACC_AGE_IND"			=> "LESS_30",
		"PAYMENT_ACC_IND"			=> "LESS_30",
		"CH_ACC_CHANGE_IND"			=> "LESS_30",
		"CH_ACC_CHANGE"				=> $datosACS["hoy"],
		"CH_ACC_DATE"				=> $datosACS["hoy"],
		"PAYMENT_ACC_AGE"			=> $datosACS["hoy"],
		"TXN_ACTIVITY_DAY"			=> 1,
		"TXN_ACTIVITY_YEAR"			=> 10,
		"SHIP_NAME_INDICATOR"		=> "DIFFERENT"
	);

 	$acs = array();
 	$acs['CARDHOLDER'] = $cardholder;
 	$acs['PURCHASE'] = $purchase;
 	$acs['MERCHANT_RISK_IND'] = $merchant_risk;
    $acs['ACCOUNT_INFO'] = $account_info;
     
    $acsready = urlencode(json_encode( $acs ));
    $firmaacs20 = makeHash($acsready, false);







    /*
    echo "<hr /><h3>Firma</h3>";
    echo $firma;
    
    echo "<hr /><h3>ACS20</h3>";
    print_r($acs);
     
    echo "<hr /><h3>Datos encripatados</h3>";
    echo $acsready;
    
    echo "<hr /><h3>Firma datos acs20</h3>";
    echo $firmaacs20;
    */
     



    
    $url = 'https://tpv.ceca.es/tpvweb/tpv/compra.action';

    $curlData = array(
        "MerchantID"        => $configuracion["MerchantID"],
        "AcquirerBIN"       => $configuracion["AcquirerBIN"],
        "TerminalID"        => $configuracion["TerminalID"],
        "URL_OK"            => $configuracion["URLOK"],
        "URL_NOK"           => $configuracion["URLNOK"],
        "Cifrado"           => $configuracion["Cifrado"],
        "Num_operacion"     => $configuracion["Transaccion"],
        "Importe"           => $configuracion["Importe"],
        "TipoMoneda"        => $configuracion["TipoMoneda"],
        "Exponente"         => $configuracion["Exponente"],
        "Pago_soportado"    => "SSL",
        "Idioma"            => "1",
        "Firma"             => $firma,
        "datos_acs_20"      => $acsready,
        "firma_acs_20"      => $firmaacs20,
        "Descripcion"       => "Pago del pedido ".$configuracion["Transaccion"]
    );

    
    /*
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, true, $context);
    if ($result === FALSE) {  }

    var_dump($result);
    */


    /*
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curlData);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

    $result = curl_exec ($curl);
    curl_close ($curl);
    */


?>


<form id="myForm" action="https://tpv.ceca.es/tpvweb/tpv/compra.action" method="post">
<h3 style="font-family:Arial">Procesando...</h3>
<?php
    foreach ($curlData as $a => $b) {
        echo '<input type="hidden" name="'.htmlentities($a).'" value="'.htmlentities($b).'">';
    }
?>
</form>
<script type="text/javascript">
    document.getElementById('myForm').submit();
</script>