<?php

    

?>


<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>Tienda de Ismaias - Página de confirmación</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script> 

    </head>
    <body>

        <header>
            <div class="collapse bg-dark" id="navbarHeader">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-7 py-4">
                            <h4 class="text-white">Tarea</h4>
                            <p class="text-muted">
                                Implementación de la pasarela de pago Cecabank para una tienda en línea básica.<br/>
                                <b>Alumno:</b> Isaías Ismael Tello Lara<br />
                                <b>Materia:</b> Modelos de negocio y métodos de pago
                            </p>
                        </div>
                        <div class="col-sm-4 offset-md-1 py-4">
                            <h4 class="text-white">Contacto</h4>
                            <ul class="list-unstyled">
                                <li><a href="./index.php" class="text-white">Regresar al carrito</a></li>
                                <li><a href="mailto:isaiasismael.telloli8@comunidadunir.net" class="text-white">Correo electrónico</a></li>
                                <li><a href="tel:+5560420790" class="text-white">Celular</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-dark bg-dark shadow-sm">
                <div class="container">
                    <a href="#" class="navbar-brand d-flex align-items-center">
                        <strong>Tienda de Ismael Tello</strong>
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </header>


        <main>
            <section class="py-5 text-center container">
                <div class="row py-lg-5">
                    <div class="col-lg-7 col-md-9 mx-auto">

                    <?php if($_GET["p"]==1) { ?> 
                        <h1 class="fw-light">Pago exitoso</h1>
                        <p class="lead text-muted">Gracias por su compra. Su número de pedido es  el <b><?php echo $_GET['t'] ?></b></p>
                        <p>
                            <a class="btn btn-success" href="./index.php">Regresar al carrito</a>
                        </p>
                    <?php } else { ?>
                        <h1 class="fw-light">Error al procesar tu pago</h1>
                        <p class="lead text-muted">Tu pago no pudo ser procesado, por favor inténtalo nuevamente regresando al carrito</p>
                        <p>
                            <a class="btn btn-danger" href="./index.php">Regresar al carrito</a>
                        </p>
                    <?php } ?>


                    </div>
                </div>
            </section>



        </main>


    </body>
</html>



